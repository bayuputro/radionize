
//jquery plugin for handle
(function($) {

	$.fn.radionize = function() {
		var target = this;
		

		target.each(function() {
			var t = $(this);

			var fieldName = t.attr('data-name');
			//create hidden input to capture radio value
			var hiddenInput = $('<input type="hidden" name="' + fieldName + '" />');
			t.append(hiddenInput);

			var options = t.find('.radio-item');

			options.each(function() {
				var o = $(this);

				//activate default value
				if (o.attr('data-checked') == 'checked') {
					hiddenInput.val(o.attr('data-value'));
					options.removeClass('active');
					o.addClass('active');
				}

				o.click(function() {
					console.log('clicked');
					options.removeClass('active');
					o.addClass('active');

					hiddenInput.val(o.attr('data-value'));
					o.attr('data-checked', 'checked');

				})
			})


		})
	};

}(jQuery));